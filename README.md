Est. 2009, Trio New American Cuisine is a Chef owned and operated group based out of Colleyville, Texas. Specializing in New American cuisine with global influences on classic dishes, Trio New American focuses on serving up the freshest local ingredients with a worldly influence.

Address: 8300 Precinct Line Rd, #104, Colleyville, TX 76034

Phone: 817-503-8440
